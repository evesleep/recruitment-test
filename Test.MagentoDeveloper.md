# Eve Backend Developer (Magento) Recruitment Test
Thank you for taking the time to do our technical test. It consists for two parts:

* [A technical test](#technical-test)
* [A couple of technical questions](#technical-questions)

You will be assigned this test by either your recruiter, or a member of Eve's deveopment team, please email your results to them directly.

Please make this a single zip file named `{yourname}-backend-developer-(magento).zip` containing:

1. A single markdown file with the answers to the technical questions
2. One folder containing the technical test

## Technical Test
* Your task is to create a Magento 2 module to extend the cart sales rule functionality, you should add a yes/no toggle to the Marketing > Cart Price Rules configuration, preferably as part of the 'Action' section, called 'Allow Financing'.
* The value should be surfaced on the following default REST endpoints:
    - /V1/quest-carts/:cartId/items
    - /V1/carts/mine/items
    - /V1/carts/:cartId/items
* We want to see you demonstrate a deep understanding of Magento 2.

___Please note:___ _there is no time limit to this test, spend as much or as little time on it as you wish._

### Requirements
* Use Magento 2 Coding Standard & best practice.
* Use Magento 2 Community Edition


### Bonus Points
* Use a TDD approach to development.

## Technical Questions
Please answer the following questions in a markdown file called `Answers to technical questions.md`

1. How long did you spend on the coding test? What would you add if you had more time? If you didn't spend much time on it, use this to explain what you would have done.
2. What do you think are the best and worst features of Magento 2, why?
3. What is your favourite Magento 2 feature, why?
4. Describe yourself in JSON.

__Thanks for your time!__